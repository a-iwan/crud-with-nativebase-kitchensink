import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Label,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text
} from "native-base";
import styles from "./styles";
import axios from "axios";
const global = require('./../api/api');
const host = global.BASE_URL;
class CrudAdd extends Component {
    constructor()
    {
      super();
      this.state = {
        s_id:'',
        s_name:'',
        s_address:'',
        s_phone:''
      }
    }
    componentWillMount() {
        const {params} = this.props.navigation.state;
        if(params!=""&&params!=null)
        {
          axios.get(host+'/person/edit/'+params.itemId)
            .then(res => {
                this.setState({s_id: res.data.person[0].id});
                this.setState({s_name: res.data.person[0].name});
                this.setState({s_address: res.data.person[0].address});
                this.setState({s_phone: res.data.person[0].phone});
            })
        }
        else{
            this.setState({s_id: ""});
            this.setState({s_name: ""});
            this.setState({s_address: ""});
            this.setState({s_phone: ""});
        }
      }

      async add_person()
      {
          if(this.state.s_id != ""){
            axios.put(host+'/person/update/', {
                id:this.state.s_id,
                name:this.state.s_name,
                address:this.state.s_address,
                phone:this.state.s_phone
            }).then(res => {
                const { navigate } = this.props.navigation;
                navigate('CrudList')
            });
          }
          else{
            axios.post(host+'/person/add/', {
                name:this.state.s_name,
                address:this.state.s_address,
                phone:this.state.s_phone
            }).then(res => {
                const { navigate } = this.props.navigation;
                navigate('CrudList')
            });
        }
      }
      async delete_person()
      {
          if(this.state.s_id != ""){
              axios.post(host+'/person/delete_person/', {id:this.state.s_id}).then(res => {const { navigate } = this.props.navigation;navigate('CrudList')});
          }
      }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>CRUD ADD</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Name</Label>
              <Input  onChangeText={ (text)=> this.setState({s_name: text}) } value={this.state.s_name} />
            </Item>
            <Item stackedLabel last>
              <Label>Address</Label>
              <Input  onChangeText={ (text)=> this.setState({s_address: text}) } value={this.state.s_address} />
            </Item>
            <Item stackedLabel last>
              <Label>Phone</Label>
              <Input  onChangeText={ (text)=> this.setState({s_phone: text}) } value={this.state.s_phone}  />
            </Item>
          </Form>
          <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {this.add_person()}}>
            <Text>Save</Text>
          </Button>
          <Button block style={{ margin: 15, marginTop: 0 }} onPress={() => {this.delete_person()}}  value={this.state.s_id} >
            <Text>Delete</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default CrudAdd;
