import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Footer,
  FooterTab,
  Left,
  Right,
  Body,
  List,
  ListItem
} from "native-base";

import styles from "./styles";
import axios from "axios";
const global = require('./../api/api');
const host = global.BASE_URL;
const datas = [
  {
    text: "Basic List"
  },
  {
    text: "ListItem Selected"
  },
  {
    text: "List Divider"
  },
  {
    text: "List Header"
  },
  {
    text: "List Avatar"
  },
  {
    text: "List Thumbnail"
  },
  {
    text: "List Separator"
  },
  {
    text: "List NoIndent"
  }
];
class CrudList extends Component {
  constructor()
  {
    super();
    this.state = {
      identifier :'',
      password :'',
      validasi :'',
      error :'',
      
      listperson:[],
      isLoggenIn: "",
      showProgress: false
    }
  }
  componentWillMount() {
    axios.get(host+'/Person')
        .then(res => {
          const listperson = res.data.person;
          this.setState({ listperson });
        })
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="ios-menu" />
            </Button>
          </Left>
          <Body>
            <Title>CRUD</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <List
            dataArray={this.state.listperson}
            renderRow={data =>
              <ListItem
                button
                onPress={() => {
                  this.props.navigation.navigate('CrudAdd', {
                    itemId: data.id,
                  });
                }}
              >
                <Left>
                  <Text>
                    {data.name}
                  </Text>
                </Left>
                <Right>
                  <Icon name="arrow-forward" />
                </Right>
              </ListItem>}
          />
        </Content>

        <Footer>
          <FooterTab>
            <Button active full  onPress={() => this.props.navigation.navigate("CrudAdd")} >
              <Text>CrudAdd</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default CrudList;
